import request from '@/utils/request'

// 查询文章和分类关联列表
export function listPostCategory(query) {
  return request({
    url: '/blog/postCategory/list',
    method: 'get',
    params: query
  })
}

// 查询文章和分类关联详细
export function getPostCategory(postId) {
  return request({
    url: '/blog/postCategory/' + postId,
    method: 'get'
  })
}

// 新增文章和分类关联
export function addPostCategory(data) {
  return request({
    url: '/blog/postCategory',
    method: 'post',
    data: data
  })
}

// 修改文章和分类关联
export function updatePostCategory(data) {
  return request({
    url: '/blog/postCategory',
    method: 'put',
    data: data
  })
}

// 删除文章和分类关联
export function delPostCategory(postId) {
  return request({
    url: '/blog/postCategory/' + postId,
    method: 'delete'
  })
}
