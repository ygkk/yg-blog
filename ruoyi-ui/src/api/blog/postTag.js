import request from '@/utils/request'

// 查询文章和标签关联列表
export function listPostTag(query) {
  return request({
    url: '/blog/postTag/list',
    method: 'get',
    params: query
  })
}

// 查询文章和标签关联详细
export function getPostTag(postId) {
  return request({
    url: '/blog/postTag/' + postId,
    method: 'get'
  })
}

// 新增文章和标签关联
export function addPostTag(data) {
  return request({
    url: '/blog/postTag',
    method: 'post',
    data: data
  })
}

// 修改文章和标签关联
export function updatePostTag(data) {
  return request({
    url: '/blog/postTag',
    method: 'put',
    data: data
  })
}

// 删除文章和标签关联
export function delPostTag(postId) {
  return request({
    url: '/blog/postTag/' + postId,
    method: 'delete'
  })
}
