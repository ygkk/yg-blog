/*
 * @Description: 
 * @Author: YUSHIJIE
 * @Date: 2024-10-11 10:55:04
 * @LastEditTime: 2024-10-17 15:37:53
 * @LastEditors: YUSHIJIE
 * @Usage: 
 */
import { createWebHashHistory, createRouter } from 'vue-router'

import HomeView from '../pages/main/index.vue'
import PostDetail from '../pages/post-detail/index.vue'
import Archives from '../pages/archives/index.vue'

const routes = [
    { path: '/', component: HomeView },
    { path: '/archives', component: Archives },
    { path: '/post/:id', component: PostDetail },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes,
})

export {
    router
}