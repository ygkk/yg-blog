/*
 * @Description: 
 * @Author: YUSHIJIE
 * @Date: 2024-10-10 17:31:25
 * @LastEditTime: 2024-10-14 14:59:53
 * @LastEditors: YUSHIJIE
 * @Usage: 
 */
import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import { router } from './routes/index.js'
import ElementPlus from 'element-plus'
import renderMarkdown from './directives/renderMarkdown.js'

const app = createApp(App)
app.use(router)
app.use(ElementPlus)
app.directive('renderMarkdown', renderMarkdown)
app.mount('#app')
