/*
 * @Description: 
 * @Author: YUSHIJIE
 * @Date: 2024-10-12 17:00:31
 * @LastEditTime: 2024-10-17 17:29:20
 * @LastEditors: YUSHIJIE
 * @Usage: 
 */
import request from '@/utils/request'

// 查询文章列表
export function listPost(query) {
    return request({
        url: '/blog/post/v/list',
        method: 'get',
        params: query
    })
}

// 查询全部文章列表
export function listAllPost(query) {
    return request({
        url: '/blog/post/v/list/all',
        method: 'get',
        params: query
    })
}

// 查询文章列表
export function postDetail(id) {
    return request({
        url: '/blog/post/v/' + id,
        method: 'get',
    })
}