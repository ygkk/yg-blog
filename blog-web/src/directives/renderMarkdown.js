/*
 * @Description: 
 * @Author: YUSHIJIE
 * @Date: 2024-10-14 10:41:32
 * @LastEditTime: 2024-10-14 15:40:44
 * @LastEditors: YUSHIJIE
 * @Usage: 
 */
import MarkDownIt from 'markdown-it'
import hljs from 'highlight.js'
import "github-markdown-css/github-markdown.css"
// import 'highlight.js/styles/github-dark.css'
import 'highlight.js/styles/atom-one-dark.css'

function renderMarkdown(el, bind) {
	const { value } = bind

	// 处理缓存情况
	if (!value) return
	const md = new MarkDownIt({
		linkify: true,
		typographer: true,
		highlight(str, lang) {
			// return hljs.highlight(str, { language: lang }).value
			if (lang && hljs.getLanguage(lang)) {
				try {
					return '<pre class="hljs"><code>' +
						hljs.highlight(lang, str, true).value +
						'</code></pre>';
				} catch (__) { }
			}

			return '<pre class="hljs"><code>' + md.utils.escapeHtml(str) + '</code></pre>';
		},
	});
	const mdTxt = md.render(value)

	// github-markdown-css主题的类名
	el.classList.add('markdown-body')
	el.innerHTML = mdTxt
}

export default {
	mounted(el, bind) {
		renderMarkdown(el, bind)
	},
	updated(el, bind) {
		renderMarkdown(el, bind)
	}
};
