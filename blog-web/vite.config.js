/*
 * @Description: 
 * @Author: YUSHIJIE
 * @Date: 2024-10-10 17:31:25
 * @LastEditTime: 2024-10-12 17:04:23
 * @LastEditors: YUSHIJIE
 * @Usage: 
 */
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'

function _resolve(dir) {
  return path.resolve(__dirname, dir)
}

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': _resolve('src')
    }
  },
  server: {
    proxy: {
      '/blog-api': {
        target: 'http://localhost:8080',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/blog-api/, '  ')
      }
    }
  }
})
