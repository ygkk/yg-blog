-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章和分类关联', '2006', '1', 'postCategory', 'blog/postCategory/index', 1, 0, 'C', '0', '0', 'blog:postCategory:list', '#', 'admin', sysdate(), '', null, '文章和分类关联菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章和分类关联查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'blog:postCategory:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章和分类关联新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'blog:postCategory:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章和分类关联修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'blog:postCategory:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章和分类关联删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'blog:postCategory:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章和分类关联导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'blog:postCategory:export',       '#', 'admin', sysdate(), '', null, '');