-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章', '3', '1', 'post', 'blog/post/index', 1, 0, 'C', '0', '0', 'blog:post:list', '#', 'admin', sysdate(), '', null, '文章菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'blog:post:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'blog:post:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'blog:post:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'blog:post:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'blog:post:export',       '#', 'admin', sysdate(), '', null, '');