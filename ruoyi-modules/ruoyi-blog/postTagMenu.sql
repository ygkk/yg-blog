-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章和标签关联', '2006', '1', 'postTag', 'blog/postTag/index', 1, 0, 'C', '0', '0', 'blog:postTag:list', '#', 'admin', sysdate(), '', null, '文章和标签关联菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章和标签关联查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'blog:postTag:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章和标签关联新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'blog:postTag:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章和标签关联修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'blog:postTag:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章和标签关联删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'blog:postTag:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('文章和标签关联导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'blog:postTag:export',       '#', 'admin', sysdate(), '', null, '');