package com.ruoyi.blog.service;

import java.util.List;
import com.ruoyi.blog.domain.BlogPostCategory;

/**
 * 文章和分类关联Service接口
 * 
 * @author yg
 * @date 2024-10-16
 */
public interface IBlogPostCategoryService 
{
    /**
     * 查询文章和分类关联
     * 
     * @param postId 文章和分类关联主键
     * @return 文章和分类关联
     */
    public BlogPostCategory selectBlogPostCategoryByPostId(Long postId);

    /**
     * 查询文章和分类关联列表
     * 
     * @param blogPostCategory 文章和分类关联
     * @return 文章和分类关联集合
     */
    public List<BlogPostCategory> selectBlogPostCategoryList(BlogPostCategory blogPostCategory);

    /**
     * 新增文章和分类关联
     * 
     * @param blogPostCategory 文章和分类关联
     * @return 结果
     */
    public int insertBlogPostCategory(BlogPostCategory blogPostCategory);

    /**
     * 修改文章和分类关联
     * 
     * @param blogPostCategory 文章和分类关联
     * @return 结果
     */
    public int updateBlogPostCategory(BlogPostCategory blogPostCategory);

    /**
     * 批量删除文章和分类关联
     * 
     * @param postIds 需要删除的文章和分类关联主键集合
     * @return 结果
     */
    public int deleteBlogPostCategoryByPostIds(Long[] postIds);

    /**
     * 删除文章和分类关联信息
     * 
     * @param postId 文章和分类关联主键
     * @return 结果
     */
    public int deleteBlogPostCategoryByPostId(Long postId);
}
