package com.ruoyi.blog.mapper;

import java.util.List;
import com.ruoyi.blog.domain.BlogPost;

/**
 * 文章Mapper接口
 * 
 * @author ruoyi
 * @date 2024-10-11
 */
public interface BlogPostMapper 
{
    /**
     * 查询文章
     * 
     * @param postId 文章主键
     * @return 文章
     */
    public BlogPost selectBlogPostByPostId(Long postId);

    /**
     * 查询文章列表
     * 
     * @param blogPost 文章
     * @return 文章集合
     */
    public List<BlogPost> selectBlogPostList(BlogPost blogPost);

    /**
     * 新增文章
     * 
     * @param blogPost 文章
     * @return 结果
     */
    public int insertBlogPost(BlogPost blogPost);

    /**
     * 修改文章
     * 
     * @param blogPost 文章
     * @return 结果
     */
    public int updateBlogPost(BlogPost blogPost);

    /**
     * 删除文章
     * 
     * @param postId 文章主键
     * @return 结果
     */
    public int deleteBlogPostByPostId(Long postId);

    /**
     * 批量删除文章
     * 
     * @param postIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBlogPostByPostIds(Long[] postIds);
}
