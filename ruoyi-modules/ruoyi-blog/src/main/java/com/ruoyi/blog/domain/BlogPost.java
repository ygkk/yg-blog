package com.ruoyi.blog.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

import java.util.List;
import java.util.Set;

/**
 * 文章对象 blog_post
 * 
 * @author ruoyi
 * @date 2024-10-11
 */
public class BlogPost extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 文章id */
    private Long postId;

    /** 文章标题 */
    @Excel(name = "文章标题")
    private String title;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 处理前的内容 */
    @Excel(name = "处理前的内容")
    private String preHandleText;

    /** 访问量 */
    @Excel(name = "访问量")
    private Long viewCount;

    /** 点赞量 */
    @Excel(name = "点赞量")
    private Long praiseCount;

    /** 收藏量 */
    @Excel(name = "收藏量")
    private Long collectCount;

    /** 文章状态（0正常 1停用） */
    @Excel(name = "文章状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    private List<BlogTag> tags;

    private BlogCategory category;

    public void setPostId(Long postId) 
    {
        this.postId = postId;
    }

    public Long getPostId() 
    {
        return postId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setPreHandleText(String preHandleText) 
    {
        this.preHandleText = preHandleText;
    }

    public String getPreHandleText() 
    {
        return preHandleText;
    }
    public void setViewCount(Long viewCount) 
    {
        this.viewCount = viewCount;
    }

    public Long getViewCount() 
    {
        return viewCount;
    }
    public void setPraiseCount(Long praiseCount) 
    {
        this.praiseCount = praiseCount;
    }

    public Long getPraiseCount() 
    {
        return praiseCount;
    }
    public void setCollectCount(Long collectCount) 
    {
        this.collectCount = collectCount;
    }

    public Long getCollectCount() 
    {
        return collectCount;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("postId", getPostId())
            .append("title", getTitle())
            .append("content", getContent())
            .append("preHandleText", getPreHandleText())
            .append("viewCount", getViewCount())
            .append("praiseCount", getPraiseCount())
            .append("collectCount", getCollectCount())
            .append("status", getStatus())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }

    public List<BlogTag> getTags() {
        return tags;
    }

    public void setTags(List<BlogTag> tags) {
        this.tags = tags;
    }

    public BlogCategory getCategory() {
        return category;
    }

    public void setCategory(BlogCategory category) {
        this.category = category;
    }
}
