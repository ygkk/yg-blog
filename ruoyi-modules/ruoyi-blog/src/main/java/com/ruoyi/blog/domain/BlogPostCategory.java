package com.ruoyi.blog.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 文章和分类关联对象 blog_post_category
 * 
 * @author yg
 * @date 2024-10-16
 */
public class BlogPostCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 文章ID */
    private Long postId;

    /** 分类ID */
    private Long categoryId;

    public void setPostId(Long postId) 
    {
        this.postId = postId;
    }

    public Long getPostId() 
    {
        return postId;
    }
    public void setCategoryId(Long categoryId) 
    {
        this.categoryId = categoryId;
    }

    public Long getCategoryId() 
    {
        return categoryId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("postId", getPostId())
            .append("categoryId", getCategoryId())
            .toString();
    }
}
