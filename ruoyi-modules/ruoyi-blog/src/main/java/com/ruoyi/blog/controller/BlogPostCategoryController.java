package com.ruoyi.blog.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.blog.domain.BlogPostCategory;
import com.ruoyi.blog.service.IBlogPostCategoryService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 文章和分类关联Controller
 * 
 * @author yg
 * @date 2024-10-16
 */
@RestController
@RequestMapping("/postCategory")
public class BlogPostCategoryController extends BaseController
{
    @Autowired
    private IBlogPostCategoryService blogPostCategoryService;

    /**
     * 查询文章和分类关联列表
     */
    @RequiresPermissions("blog:postCategory:list")
    @GetMapping("/list")
    public TableDataInfo list(BlogPostCategory blogPostCategory)
    {
        startPage();
        List<BlogPostCategory> list = blogPostCategoryService.selectBlogPostCategoryList(blogPostCategory);
        return getDataTable(list);
    }

    /**
     * 导出文章和分类关联列表
     */
    @RequiresPermissions("blog:postCategory:export")
    @Log(title = "文章和分类关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BlogPostCategory blogPostCategory)
    {
        List<BlogPostCategory> list = blogPostCategoryService.selectBlogPostCategoryList(blogPostCategory);
        ExcelUtil<BlogPostCategory> util = new ExcelUtil<BlogPostCategory>(BlogPostCategory.class);
        util.exportExcel(response, list, "文章和分类关联数据");
    }

    /**
     * 获取文章和分类关联详细信息
     */
    @RequiresPermissions("blog:postCategory:query")
    @GetMapping(value = "/{postId}")
    public AjaxResult getInfo(@PathVariable("postId") Long postId)
    {
        return success(blogPostCategoryService.selectBlogPostCategoryByPostId(postId));
    }

    /**
     * 新增文章和分类关联
     */
    @RequiresPermissions("blog:postCategory:add")
    @Log(title = "文章和分类关联", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BlogPostCategory blogPostCategory)
    {
        return toAjax(blogPostCategoryService.insertBlogPostCategory(blogPostCategory));
    }

    /**
     * 修改文章和分类关联
     */
    @RequiresPermissions("blog:postCategory:edit")
    @Log(title = "文章和分类关联", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BlogPostCategory blogPostCategory)
    {
        return toAjax(blogPostCategoryService.updateBlogPostCategory(blogPostCategory));
    }

    /**
     * 删除文章和分类关联
     */
    @RequiresPermissions("blog:postCategory:remove")
    @Log(title = "文章和分类关联", businessType = BusinessType.DELETE)
	@DeleteMapping("/{postIds}")
    public AjaxResult remove(@PathVariable Long[] postIds)
    {
        return toAjax(blogPostCategoryService.deleteBlogPostCategoryByPostIds(postIds));
    }
}
