package com.ruoyi.blog.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.ruoyi.blog.domain.*;
import com.ruoyi.blog.mapper.*;
import com.ruoyi.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.blog.service.IBlogPostService;
import org.yaml.snakeyaml.Yaml;

/**
 * 文章Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-10-11
 */
@Service
public class BlogPostServiceImpl implements IBlogPostService 
{
    @Autowired
    private BlogPostMapper blogPostMapper;
    @Autowired
    private BlogTagMapper blogTagMapper;
    @Autowired
    private BlogPostTagMapper blogPostTagMapper;
    @Autowired
    private BlogCategoryMapper blogCategoryMapper;
    @Autowired
    private BlogPostCategoryMapper blogPostCategoryMapper;

    /**
     * 查询文章
     * 
     * @param postId 文章主键
     * @return 文章
     */
    @Override
    public BlogPost selectBlogPostByPostId(Long postId)
    {
        return blogPostMapper.selectBlogPostByPostId(postId);
    }

    /**
     * 查询文章列表
     * 
     * @param blogPost 文章
     * @return 文章
     */
    @Override
    public List<BlogPost> selectBlogPostList(BlogPost blogPost)
    {
        return blogPostMapper.selectBlogPostList(blogPost);
    }

    /**
     * 新增文章
     * 
     * @param blogPost 文章
     * @return 结果
     */
    @Override
    public int insertBlogPost(BlogPost blogPost)
    {
        String preHandleText = blogPost.getPreHandleText();
        System.out.println(preHandleText);
        // 解析preHandleText的信息，包含输入：作者、创建时间、标签、类别、点赞数、收藏数、阅读数
        Yaml yaml = new Yaml();
        Map<String, Object> yamlMap = yaml.load(preHandleText);
        System.out.println(yamlMap);
        System.out.println(yamlMap.get("tags"));
        String author = (String) yamlMap.get("author");
        if (null != author) {
            blogPost.setCreateBy(author);
        }
        blogPost.setCreateTime(DateUtils.getNowDate());
        try {
            Date time = (Date) yamlMap.get("createTime");
            if (time != null) {
                blogPost.setCreateTime(time);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        int insertResultRow = blogPostMapper.insertBlogPost(blogPost);
        // 处理标签数据
        String tagName = (String) yamlMap.get("tag");
        List<String> tagNames = (ArrayList) yamlMap.get("tags");
        if (null != tagName && tagNames == null) {
            // 只配置了tag，使用tag， tags优先级高于tag
            concatPostTag(tagName, blogPost.getPostId());
        }
        if (tagNames != null) {
            for (String tagNameItem: tagNames) {
                concatPostTag(tagNameItem, blogPost.getPostId());
            }
        }
        // 处理分类数据
        String categoryName = (String) yamlMap.get("category");
        if (null != categoryName) {
            concatPostCategory(categoryName, blogPost.getPostId());
        }
        return insertResultRow;
    }

    private void concatPostTag(String tagName, Long postId) {
        if (tagName == null) {
            return;
        }
        BlogTag blogTag = blogTagMapper.selectBlogTagByTagName(tagName);
        Long tagId = null;
        if (null == blogTag) {
            // 标签未存在，创建标签
            blogTag = new BlogTag();
            blogTag.setName(tagName);
            int i = blogTagMapper.insertBlogTag(blogTag);
            if (i >= 0) {
                tagId = blogTag.getTagId();
            }
        } else {
            tagId = blogTag.getTagId();
        }
        if (null != tagId) {
            // 关联标签
            BlogPostTag blogPostTag = new BlogPostTag();
            blogPostTag.setPostId(postId);
            blogPostTag.setTagId(tagId);
            blogPostTag.setCreateTime(DateUtils.getNowDate());
            blogPostTagMapper.insertBlogPostTag(blogPostTag);
        }
    }

    private void concatPostCategory(String categoryName, Long postId) {
        if (categoryName == null) {
            return;
        }
        BlogCategory blogCategory = blogCategoryMapper.selectBlogCategoryByCategoryName(categoryName);
        Long categoryId = null;
        if (null == blogCategory) {
            // 标签未存在，创建标签
            blogCategory = new BlogCategory();
            blogCategory.setName(categoryName);
            int i = blogCategoryMapper.insertBlogCategory(blogCategory);
            if (i >= 0) {
                categoryId = blogCategory.getCategoryId();
            }
        } else {
            categoryId = blogCategory.getCategoryId();
        }
        if (null != categoryId) {
            // 关联标签
            BlogPostCategory blogPostCategory = new BlogPostCategory();
            blogPostCategory.setPostId(postId);
            blogPostCategory.setCategoryId(categoryId);
            blogPostCategory.setCreateTime(DateUtils.getNowDate());
            blogPostCategoryMapper.insertBlogPostCategory(blogPostCategory);
        }
    }

    /**
     * 修改文章
     * 
     * @param blogPost 文章
     * @return 结果
     */
    @Override
    public int updateBlogPost(BlogPost blogPost)
    {
        blogPost.setUpdateTime(DateUtils.getNowDate());
        return blogPostMapper.updateBlogPost(blogPost);
    }

    /**
     * 批量删除文章
     * 
     * @param postIds 需要删除的文章主键
     * @return 结果
     */
    @Override
    public int deleteBlogPostByPostIds(Long[] postIds)
    {
        return blogPostMapper.deleteBlogPostByPostIds(postIds);
    }

    /**
     * 删除文章信息
     * 
     * @param postId 文章主键
     * @return 结果
     */
    @Override
    public int deleteBlogPostByPostId(Long postId)
    {
        return blogPostMapper.deleteBlogPostByPostId(postId);
    }
}
