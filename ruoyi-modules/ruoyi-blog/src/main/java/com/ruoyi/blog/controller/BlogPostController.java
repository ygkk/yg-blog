package com.ruoyi.blog.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.utils.sql.SqlUtil;
import com.ruoyi.common.core.web.page.PageDomain;
import com.ruoyi.common.core.web.page.TableSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.blog.domain.BlogPost;
import com.ruoyi.blog.service.IBlogPostService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 文章Controller
 * 
 * @author ruoyi
 * @date 2024-10-11
 */
@RestController
@RequestMapping("/post")
public class BlogPostController extends BaseController
{
    @Autowired
    private IBlogPostService blogPostService;

    /**
     * 查询文章列表
     */
    @RequiresPermissions("blog:post:list")
    @GetMapping("/list")
    public TableDataInfo list(BlogPost blogPost)
    {
        startPage();
        List<BlogPost> list = blogPostService.selectBlogPostList(blogPost);
        return getDataTable(list);
    }

    /**
     * 查询文章列表
     */
    @GetMapping("/v/list")
    public TableDataInfo clientList(BlogPost blogPost)
    {
        startPage();
        List<BlogPost> list = blogPostService.selectBlogPostList(blogPost);
        System.out.println(list.size());
        return getDataTable(list);
    }

    /**
     * 客户端查询全部文章列表
     */
    @GetMapping("/v/list/all")
    public AjaxResult clientAllList(BlogPost blogPost)
    {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
        System.out.println(orderBy);
        List<BlogPost> list = blogPostService.selectBlogPostList(blogPost);
        return success(list);
    }

    /**
     * 导出文章列表
     */
    @RequiresPermissions("blog:post:export")
    @Log(title = "文章", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BlogPost blogPost)
    {
        List<BlogPost> list = blogPostService.selectBlogPostList(blogPost);
        ExcelUtil<BlogPost> util = new ExcelUtil<BlogPost>(BlogPost.class);
        util.exportExcel(response, list, "文章数据");
    }

    /**
     * 获取文章详细信息
     */
    @RequiresPermissions("blog:post:query")
    @GetMapping(value = "/{postId}")
    public AjaxResult getInfo(@PathVariable("postId") Long postId)
    {
        return success(blogPostService.selectBlogPostByPostId(postId));
    }

    /**
     * 获取文章详细信息
     */
    @GetMapping(value = "/v/{postId}")
    public AjaxResult visitorGetInfo(@PathVariable("postId") Long postId)
    {
        return success(blogPostService.selectBlogPostByPostId(postId));
    }

    /**
     * 新增文章
     */
    @RequiresPermissions("blog:post:add")
    @Log(title = "文章", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BlogPost blogPost)
    {
        return toAjax(blogPostService.insertBlogPost(blogPost));
    }

    /**
     * 修改文章
     */
    @RequiresPermissions("blog:post:edit")
    @Log(title = "文章", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BlogPost blogPost)
    {
        return toAjax(blogPostService.updateBlogPost(blogPost));
    }

    /**
     * 删除文章
     */
    @RequiresPermissions("blog:post:remove")
    @Log(title = "文章", businessType = BusinessType.DELETE)
	@DeleteMapping("/{postIds}")
    public AjaxResult remove(@PathVariable Long[] postIds)
    {
        return toAjax(blogPostService.deleteBlogPostByPostIds(postIds));
    }
}
