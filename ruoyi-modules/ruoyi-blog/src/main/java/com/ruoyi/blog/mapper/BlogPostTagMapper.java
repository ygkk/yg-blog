package com.ruoyi.blog.mapper;

import java.util.List;
import com.ruoyi.blog.domain.BlogPostTag;

/**
 * 文章和标签关联Mapper接口
 * 
 * @author yg
 * @date 2024-10-16
 */
public interface BlogPostTagMapper 
{
    /**
     * 查询文章和标签关联
     * 
     * @param postId 文章和标签关联主键
     * @return 文章和标签关联
     */
    public BlogPostTag selectBlogPostTagByPostId(Long postId);

    /**
     * 查询文章和标签关联列表
     * 
     * @param blogPostTag 文章和标签关联
     * @return 文章和标签关联集合
     */
    public List<BlogPostTag> selectBlogPostTagList(BlogPostTag blogPostTag);

    /**
     * 新增文章和标签关联
     * 
     * @param blogPostTag 文章和标签关联
     * @return 结果
     */
    public int insertBlogPostTag(BlogPostTag blogPostTag);

    /**
     * 修改文章和标签关联
     * 
     * @param blogPostTag 文章和标签关联
     * @return 结果
     */
    public int updateBlogPostTag(BlogPostTag blogPostTag);

    /**
     * 删除文章和标签关联
     * 
     * @param postId 文章和标签关联主键
     * @return 结果
     */
    public int deleteBlogPostTagByPostId(Long postId);

    /**
     * 批量删除文章和标签关联
     * 
     * @param postIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBlogPostTagByPostIds(Long[] postIds);
}
