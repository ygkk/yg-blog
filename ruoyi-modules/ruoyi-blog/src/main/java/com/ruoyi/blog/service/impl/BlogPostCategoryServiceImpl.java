package com.ruoyi.blog.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.blog.mapper.BlogPostCategoryMapper;
import com.ruoyi.blog.domain.BlogPostCategory;
import com.ruoyi.blog.service.IBlogPostCategoryService;

/**
 * 文章和分类关联Service业务层处理
 * 
 * @author yg
 * @date 2024-10-16
 */
@Service
public class BlogPostCategoryServiceImpl implements IBlogPostCategoryService 
{
    @Autowired
    private BlogPostCategoryMapper blogPostCategoryMapper;

    /**
     * 查询文章和分类关联
     * 
     * @param postId 文章和分类关联主键
     * @return 文章和分类关联
     */
    @Override
    public BlogPostCategory selectBlogPostCategoryByPostId(Long postId)
    {
        return blogPostCategoryMapper.selectBlogPostCategoryByPostId(postId);
    }

    /**
     * 查询文章和分类关联列表
     * 
     * @param blogPostCategory 文章和分类关联
     * @return 文章和分类关联
     */
    @Override
    public List<BlogPostCategory> selectBlogPostCategoryList(BlogPostCategory blogPostCategory)
    {
        return blogPostCategoryMapper.selectBlogPostCategoryList(blogPostCategory);
    }

    /**
     * 新增文章和分类关联
     * 
     * @param blogPostCategory 文章和分类关联
     * @return 结果
     */
    @Override
    public int insertBlogPostCategory(BlogPostCategory blogPostCategory)
    {
        return blogPostCategoryMapper.insertBlogPostCategory(blogPostCategory);
    }

    /**
     * 修改文章和分类关联
     * 
     * @param blogPostCategory 文章和分类关联
     * @return 结果
     */
    @Override
    public int updateBlogPostCategory(BlogPostCategory blogPostCategory)
    {
        return blogPostCategoryMapper.updateBlogPostCategory(blogPostCategory);
    }

    /**
     * 批量删除文章和分类关联
     * 
     * @param postIds 需要删除的文章和分类关联主键
     * @return 结果
     */
    @Override
    public int deleteBlogPostCategoryByPostIds(Long[] postIds)
    {
        return blogPostCategoryMapper.deleteBlogPostCategoryByPostIds(postIds);
    }

    /**
     * 删除文章和分类关联信息
     * 
     * @param postId 文章和分类关联主键
     * @return 结果
     */
    @Override
    public int deleteBlogPostCategoryByPostId(Long postId)
    {
        return blogPostCategoryMapper.deleteBlogPostCategoryByPostId(postId);
    }
}
