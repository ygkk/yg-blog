package com.ruoyi.blog.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.log.annotation.Log;
import com.ruoyi.common.log.enums.BusinessType;
import com.ruoyi.common.security.annotation.RequiresPermissions;
import com.ruoyi.blog.domain.BlogPostTag;
import com.ruoyi.blog.service.IBlogPostTagService;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.common.core.utils.poi.ExcelUtil;
import com.ruoyi.common.core.web.page.TableDataInfo;

/**
 * 文章和标签关联Controller
 * 
 * @author yg
 * @date 2024-10-16
 */
@RestController
@RequestMapping("/postTag")
public class BlogPostTagController extends BaseController
{
    @Autowired
    private IBlogPostTagService blogPostTagService;

    /**
     * 查询文章和标签关联列表
     */
    @RequiresPermissions("blog:postTag:list")
    @GetMapping("/list")
    public TableDataInfo list(BlogPostTag blogPostTag)
    {
        startPage();
        List<BlogPostTag> list = blogPostTagService.selectBlogPostTagList(blogPostTag);
        return getDataTable(list);
    }

    /**
     * 导出文章和标签关联列表
     */
    @RequiresPermissions("blog:postTag:export")
    @Log(title = "文章和标签关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BlogPostTag blogPostTag)
    {
        List<BlogPostTag> list = blogPostTagService.selectBlogPostTagList(blogPostTag);
        ExcelUtil<BlogPostTag> util = new ExcelUtil<BlogPostTag>(BlogPostTag.class);
        util.exportExcel(response, list, "文章和标签关联数据");
    }

    /**
     * 获取文章和标签关联详细信息
     */
    @RequiresPermissions("blog:postTag:query")
    @GetMapping(value = "/{postId}")
    public AjaxResult getInfo(@PathVariable("postId") Long postId)
    {
        return success(blogPostTagService.selectBlogPostTagByPostId(postId));
    }

    /**
     * 新增文章和标签关联
     */
    @RequiresPermissions("blog:postTag:add")
    @Log(title = "文章和标签关联", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BlogPostTag blogPostTag)
    {
        return toAjax(blogPostTagService.insertBlogPostTag(blogPostTag));
    }

    /**
     * 修改文章和标签关联
     */
    @RequiresPermissions("blog:postTag:edit")
    @Log(title = "文章和标签关联", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BlogPostTag blogPostTag)
    {
        return toAjax(blogPostTagService.updateBlogPostTag(blogPostTag));
    }

    /**
     * 删除文章和标签关联
     */
    @RequiresPermissions("blog:postTag:remove")
    @Log(title = "文章和标签关联", businessType = BusinessType.DELETE)
	@DeleteMapping("/{postIds}")
    public AjaxResult remove(@PathVariable Long[] postIds)
    {
        return toAjax(blogPostTagService.deleteBlogPostTagByPostIds(postIds));
    }
}
