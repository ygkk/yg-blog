package com.ruoyi.blog.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.blog.mapper.BlogPostTagMapper;
import com.ruoyi.blog.domain.BlogPostTag;
import com.ruoyi.blog.service.IBlogPostTagService;

/**
 * 文章和标签关联Service业务层处理
 * 
 * @author yg
 * @date 2024-10-16
 */
@Service
public class BlogPostTagServiceImpl implements IBlogPostTagService 
{
    @Autowired
    private BlogPostTagMapper blogPostTagMapper;

    /**
     * 查询文章和标签关联
     * 
     * @param postId 文章和标签关联主键
     * @return 文章和标签关联
     */
    @Override
    public BlogPostTag selectBlogPostTagByPostId(Long postId)
    {
        return blogPostTagMapper.selectBlogPostTagByPostId(postId);
    }

    /**
     * 查询文章和标签关联列表
     * 
     * @param blogPostTag 文章和标签关联
     * @return 文章和标签关联
     */
    @Override
    public List<BlogPostTag> selectBlogPostTagList(BlogPostTag blogPostTag)
    {
        return blogPostTagMapper.selectBlogPostTagList(blogPostTag);
    }

    /**
     * 新增文章和标签关联
     * 
     * @param blogPostTag 文章和标签关联
     * @return 结果
     */
    @Override
    public int insertBlogPostTag(BlogPostTag blogPostTag)
    {
        return blogPostTagMapper.insertBlogPostTag(blogPostTag);
    }

    /**
     * 修改文章和标签关联
     * 
     * @param blogPostTag 文章和标签关联
     * @return 结果
     */
    @Override
    public int updateBlogPostTag(BlogPostTag blogPostTag)
    {
        return blogPostTagMapper.updateBlogPostTag(blogPostTag);
    }

    /**
     * 批量删除文章和标签关联
     * 
     * @param postIds 需要删除的文章和标签关联主键
     * @return 结果
     */
    @Override
    public int deleteBlogPostTagByPostIds(Long[] postIds)
    {
        return blogPostTagMapper.deleteBlogPostTagByPostIds(postIds);
    }

    /**
     * 删除文章和标签关联信息
     * 
     * @param postId 文章和标签关联主键
     * @return 结果
     */
    @Override
    public int deleteBlogPostTagByPostId(Long postId)
    {
        return blogPostTagMapper.deleteBlogPostTagByPostId(postId);
    }
}
