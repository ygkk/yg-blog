package com.ruoyi.blog.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.core.web.domain.BaseEntity;

/**
 * 文章和标签关联对象 blog_post_tag
 * 
 * @author yg
 * @date 2024-10-16
 */
public class BlogPostTag extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 文章ID */
    private Long postId;

    /** 标签ID */
    private Long tagId;

    public void setPostId(Long postId) 
    {
        this.postId = postId;
    }

    public Long getPostId() 
    {
        return postId;
    }
    public void setTagId(Long tagId) 
    {
        this.tagId = tagId;
    }

    public Long getTagId() 
    {
        return tagId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("postId", getPostId())
            .append("tagId", getTagId())
            .toString();
    }
}
